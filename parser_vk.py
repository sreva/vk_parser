# --*-- coding: utf-8 --*--
import ConfigParser
import urllib, urllib2
import lxml.html
import re
from grab import Grab
from bs4 import BeautifulSoup
import re
import time
import xlrd
import xlwt
import csv
import time
import codecs
from xlutils.copy import copy as xlcopy
from grab.selector import Selector
from lxml.html import fromstring




class Vkontakte:
    def __init__(self, login, password):
        self.login = login
        self.password = password
        self.cookies = None

    def __ip_h(self):
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux i686; rv:15.0) Gecko/20100101 Firefox/15.0'}
        req = urllib2.Request('http://vk.com/', None, headers)
        response = urllib2.urlopen(req)
        ip_h = re.findall(r'value="[a-z 0-9]{18}"', response.read())[0]

        return ip_h

    def __remixsid(self, ip_h):
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux i686; rv:15.0) Gecko/20100101 Firefox/15.0'}

        values = {'act': 'login',
                  'q': '1',
                  'al_frame': '1',
                  'expire': '',
                  'captcha_sid': '',
                  'captcha_key': '',
                  'from_host': 'vk.com',
                  'from_protocol': 'http',
                  'ip_h': ip_h,
                  'email': self.login,
                  'pass': self.password}

        data = urllib.urlencode(values)
        req = urllib2.Request('https://login.vk.com/?act=login', data, headers)
        response = urllib2.urlopen(req)

        return re.findall(r"remixsid=([a-z 0-9]*);", response.headers['Set-Cookie'])[0]

    def getCookies(self):
        if self.cookies: return self.cookies

        ip_h = self.__ip_h()
        remixsid = self.__remixsid(ip_h)

        list_cookies = ['remixlang=0;',
                        'remixchk=5;',
                        'remixdt=0;',
                        'audio_vol=55;',
                        'remixflash=11.2.202;',
                        'remixseenads=2;'
                        'remixsid=' + remixsid + ';',
                        'remixreg_sid=;',
                        'remixrec_sid=']

        return " ".join(list_cookies)

    def HtmlPage(self, url):
        cookie = self.getCookies()

        header = {'User-Agent': 'Mozilla/5.0 (X11; Linux i686; rv:15.0) Gecko/20100101 Firefox/15.0',
                  'Cookie': cookie}

        req = urllib2.Request(url, None, header)

        response = urllib2.urlopen(req)

        return response.read()

config = ConfigParser.SafeConfigParser()
config.read("settings.ini", )

vk_password = config.get("config", "vk_password")
vk_login = config.get("config", "vk_login")
z = Vkontakte(vk_login, vk_password)

source_filename = "template.xls"
workbook = xlwt.Workbook()
read_book = xlrd.open_workbook(source_filename, on_demand=True)
read_sheet = read_book.get_sheet(0)
write_book = xlcopy(read_book)
write_sheet = write_book.get_sheet(0)
file = open('source.csv', 'rb')
file_line = csv.reader(file)
lines = 1
for line in file_line:
    groups_list = ''
    user_post_count = ''
    school_class = ''
    school_year = ''
    school_spec = ''
    school_end = ''
    school_name = ''
    fans_count = ''
    users_post_count = ''
    link = line[0]
    users_video_count = ''
    user_post_count = ''
    div_audio_count = ''
    div_photo_count = ''
    div_friends_count = ''
    div_photo_count = ''
    div_group_count = ''
    users_video_count = ''
    div_video = ''
    users_groups_count = ''
    sex_content = ''
    user_sex = ''
    print link
    write_sheet.write(lines, 1, link)
    write_sheet.write(lines, 0, 'vk.com')

    #    link = 'https://vk.com/kiradj.angel'
    if link.find('wall') != -1 and link.find('reply') != -1:

        contents = z.HtmlPage(link)
        soup = BeautifulSoup(contents)
        coment_content = link.split('_')
        first = coment_content[0].split('-')
        number = coment_content[1].split('=')
        div_id = ('post-%s-%s') % (first[1], number[1])
        div = soup.find('div', {"id": "post-50313747_3867"})

        soup = BeautifulSoup(str(div))
        a = soup.find("a", {'class': "fw_reply_thumb"})
        link_profile = "vk.com%s" % a['href']

    elif link.find('wall') != -1:
        link = link.replace('http://vk.com/wall', '')
        link = link.replace('https://vk.com/wall', '')
        link = link.replace('-', '')
        link = link.split('_')[0]

        link = "http://vk.com/id%s" % link.replace(' ', '')
    else:
        pass
    write_sheet.write(lines, 2, link) # Линк профиля
    print link
    g = Grab()
    #try:
    content_main = z.HtmlPage(link)
    soup_main = BeautifulSoup(content_main)
    bloced = soup_main.find('div', {'class' : 'profile_blocked'})
    error = False
    if bloced:
        error = True
    errors = soup_main.find('div', {'class': 'profile_deleted'})

    if errors:
        error = True
    group_page = soup_main.find('div', {'id': 'group_wide'})
    if group_page:
        error = True
    if not error:

        cell_uni = 65
        cells_uni = {
            u'ВУЗ:': 64,
            u'Факультет:': 65,
            u'Кафедра:': 66,
            u'Форма обучения:': 67,
            u'Статус:': 68,
        }
        content_new = soup_main.find('div', {'id': 'profile_full_info'})
        if content_new:
            h4 = content_new.findAll('h4')
            if h4:
                for elem in h4:

                    if elem.text == u'Образование':

                        main_div = elem.findNextSibling('div')
                        divs = main_div.findAll('div', {"class": "label fl_l", })
                        b = 0
                        for elem in divs:
                            if elem.text == u'ВУЗ:':
                                b += 6
                            coll = cells_uni.get(elem.text, False)
                            if coll:
                                write_sheet.write(lines, coll + b, elem.findNextSibling('div').text)

                        div_all = main_div.findChildren('div')
                        edi_soup = BeautifulSoup(str(div_all))

        soup = BeautifulSoup(content_main)
        fio = ''
        title_fio = soup.find('div', {'class': "profile_deleted"})
        if title_fio:
            fio = soup.find('h4', {'class': "simple"})
            fio = fio.text
        fio = soup.find('div', {'class': 'page_name fl_l ta_l'})
        if fio:
            fio = fio.text

        soup = BeautifulSoup(content_main)
        avatar_a = soup.find('a', {'id': 'profile_photo_link'})
        id = ''
        if avatar_a:

            try:
                avatar_a = avatar_a['href'].split('_')
                avatar_a = avatar_a[0].replace('/photo', '')
                id = avatar_a
            except:
                id = ''
        else:
            avatar_a = soup.find('div', {'id': 'profile_friends'})
            if avatar_a:
                try:
                    avatar_a = avatar_a.find('a', {'class': 'module_header'})
                    avatar_a = avatar_a['href']
                    avatar_a = avatar_a.split['&']
                    avatar_a = avatar_a[0].split('=')
                    id = avatar_a[1]
                except:
                    id = ''

            fans_count_a = soup.find('a', {'class': 'fans'})
            if fans_count_a:
                fans_count_a = str(fans_count_a.text.encode('utf-8'))
                fans_count_a = fans_count_a.split(' ')
                fans_count_a = fans_count_a[0]
                fans_count_a = re.findall('\d', str(fans_count_a))
                for el in fans_count_a:
                    fans_count += el
        print id
        if id != '':
            print id
            content = z.HtmlPage('https://vk.com/wall%s?own=1' % id)
            soup_post = BeautifulSoup(content)
            div_post = soup_post.find('div', {'id': 'fw_summary'})
            div_post_int = re.findall('\d', str(div_post.text.encode('utf-8')))

            for el in div_post_int:
                user_post_count += el
            content = z.HtmlPage('https://vk.com/wall%s' % id)
            soup_post = BeautifulSoup(content)
            div_post = soup_post.find('div', {'id': 'fw_summary'})
            if div_post:
                div_post_int = re.findall('\d', str(div_post.text.encode('utf-8')))
                users_post_count = ''

                for el in div_post_int:
                    users_post_count += el
            content = z.HtmlPage('https://vk.com/groups?id=%s' % id)
            soup_post = BeautifulSoup(content)
            div_group = soup_post.find('div', {'id': 'groups_list_summary'})
            div_group_count = soup_post.findAll('div', {'class': 'group_list_row'})
            x = 0
            for elem in div_group_count:
                x += 1
            if div_group:
                div_group = str(div_group.text.encode('utf-8'))

                div_group = div_group.split('|')
                div_group = div_group[0].split('в')
                div_groups_int = re.findall('\d', str(div_group[1]))

                for el in div_groups_int:
                    users_groups_count += el

        div_video = soup.find('div', {'id': 'profile_videos'})
        if div_video:
            div_video = div_video.find('div', {'class': 'p_header_bottom'})

            div_video_int = re.findall('\d', str(div_video.text.encode('utf-8')))
            users_video_count = ''
            for el in div_video_int:
                users_video_count += el
        div_audio = soup.find('div', {'id': 'profile_audios'})
        if div_audio:
            div_audio = div_audio.find('div', {'class': 'p_header_bottom'})

            div_audio_int = re.findall('\d', str(div_audio.text.encode('utf-8')))
            div_audio_count = ''
            for el in div_audio_int:
                div_audio_count += el
        div_friends = soup.find('div', {'id': 'profile_friends'})
        if div_friends:
            div_friends = div_friends.find('div', {'class': 'p_header_bottom'})

            div_friends_int = re.findall('\d', str(div_friends.text.encode('utf-8')))
            div_friends_count = ''
            for el in div_friends_int:
                div_friends_count += el

        div_photo = soup.find('div', {'id': 'profile_photos_module'})
        if div_photo:
            div_photo = div_photo.find('div', {'class': 'header_top clear_fix'})

            div_photo_int = re.findall('\d', str(div_photo.text.encode('utf-8')))
            div_photo_count = ''
            for el in div_photo_int:
                div_photo_count += el

        cells = {u"День рождения": 7,
                 u"Родной город": 8,
                 u"Языки": 9,
                 u"Дедушки, бабушки": 10,
                 u"Родители": 11,
                 u"Братья, сестры": 12,
                 u"Дети": 13,
                 u"Внуки": 14,
                 u"Город": 16,
                 u"Моб. телефон": 18,
                 u"Доп. телефон": 19,
                 u"Skype": 20,
                 u"Instagram": 21,
                 u"Facebook": 22,
                 u"Twitter": 23,
                 u"Веб-сайт": 24,
                 u"Деятельность": 25,
                 u"Интересы": 26,
                 u"Любимая музыка": 27,
                 u"Любимые фильмы": 28,
                 u"Любимые телешоу": 29,
                 u"Любимые книги": 30,
                 u"Любимые игры": 31,
                 u"Любимые цитаты": 32,
                 u"О себе": 33,
                 u"Полит. предпочтения": 118,
                 u"Мировоззрение": 119,
                 u"Главное в жизни": 120,
                 u"Главное в людях": 121,
                 u"Отн. к курению": 122,
                 u"Отн. к алкоголю": 123,
                 u"Вдохновляют": 124,


        }
        write_sheet.write(lines, 125, div_friends_count)
        write_sheet.write(lines, 126, fans_count)
        write_sheet.write(lines, 127, user_post_count)
        write_sheet.write(lines, 128, users_post_count)
        write_sheet.write(lines, 129, users_groups_count)
        write_sheet.write(lines, 131, div_audio_count)
        write_sheet.write(lines, 132, users_video_count)
        write_sheet.write(lines, 133, div_photo_count)
        arr = []
        cell_work = 94
        soup = BeautifulSoup(content_main)
        div = soup.findAll('div', {"class": "clear_fix "})
        i = 0
        h = 0
        for elem in div:
            soup = BeautifulSoup(str(elem))
            title = soup.find('div', {'class': 'label fl_l'})

            title = title.text
            title = title.replace(':', '')

            if title == u"Семейное положение":
                sex_content = soup.find('div', {'class': 'labeled fl_l'})
                sex_content = sex_content.text

                status = soup.find('a')
                if sex_content.find(u'не женат') != -1 or sex_content.find(u'помовлен') != -1 or sex_content.find(
                        u'женат') != -1 or sex_content.find(u'влюблён') != -1:
                    user_sex = u'Мужской'
                elif sex_content.find(u'не замужем') != -1 or sex_content.find(u'помолвлена') != -1 or sex_content.find(
                        u'замужем') != -1 or sex_content.find(u'влюблена') != -1 or sex_content.find(u'есть друг'):
                    user_sex = u'Женский'

            content = soup.findAll('a')
            contents = u''
            if content:
                for el in content:
                    if cells.get(title):
                        contents += u'%s, ' % el.text
            else:
                content = soup.find('div', {'class': 'labeled fl_l'})
                if cells.get(title):
                    contents = content.text
            if title == u"Группы":
                for elem in content:
                    groups_list += '%s, ' % elem.text
            if cells.get(title):
                write_sheet.write(lines, cells.get(title), contents)
            if title == u"ВУЗ":
                content = soup.find('a')

            if title == u"Место работы":
                work_name = ''
                work_city = ''
                work_spec = ''
                work_start_date = ''
                work_end_date = ''
                div = soup.find('div', {'class': 'labeled fl_l'})
                content = soup.find('a')

                work_name = content.text
                content_a = soup.findAll('a')
                for elem in content_a:
                    if elem.text != work_name:
                        work_spec = elem.text
                div = BeautifulSoup(str(div))
                text = div.text
                text = text.replace(work_name, '')
                text = text.replace(work_spec, '')
                if text.find(',') != -1:
                    text = text.split(',')
                    work_city = text[0]

                    work_start_date = text[1]
                    if work_start_date.find('.') == -1:

                        work_end_date = work_start_date[6:10]
                        work_start_date = work_start_date[1:5]

                    else:
                        if work_start_date.find(u'до') == -1:
                            work_end_date = ''
                            work_start_date = work_start_date
                        else:
                            work_end_date = work_start_date
                            work_start_date = ''

                else:
                    work_city = text

                write_sheet.write(lines, cell_work + h, work_name)

                write_sheet.write(lines, cell_work + 1 + h, work_start_date)
                write_sheet.write(lines, cell_work + 2 + h, work_end_date)
                write_sheet.write(lines, cell_work + 3 + h, work_spec)
                h += 4
                if title == u"Группы":
                    content = soup.find('div', {'class': 'labeled fl_l'})
                    groups = content.findAll('a')

                    for elem in groups:
                        groups_list += '%s, ' % elem.text
        soup = BeautifulSoup(content_main)
        div = soup.findAll('div', {"class": "clear_fix block"})
        cell_school = 34
        cell_uni = 65
        f = 0
        g = 6
        for elem in div:
            soup = BeautifulSoup(str(elem))
            title = soup.find('div', {'class': 'label fl_l'})
            title = title.text
            title = title.replace(':', '')
            if title == u"Школа" or title == u'Музыкальная школа' or title == u"Гимназия" or title == u"Спортивная школа" or title == u"Школа искусств" or title == u"Колледж":

                div = soup.find('div', {'class': 'labeled fl_l'})
                div = BeautifulSoup(str(div))
                text = div.text

                contents = div.findChildren('a')
                school_name = contents[0].text
                text = text.replace(school_name, '')
                for elem in contents:
                    if not elem.text.find('(') or not elem.text.find("'") or elem.text == school_name:
                        school_spec = ''

                    else:
                        school_spec = elem.text
                        text = text.replace(school_spec, '')
                    if not elem.text.find('('):
                        school_class = elem.text
                        text = text.replace(school_class, '')
                    if not elem.text.find("'"):
                        school_year = elem.text
                        text = text.replace(school_year, '')
                text = text.split(',')
                school_city = text[0]
                school_start = ''
                try:
                    text = text[1]
                    if text.find('.') == -1:
                        school_start = text[1:5]
                        school_end = text[6:10]


                    else:
                        school_start = text

                except:

                    pass

                if f < 32:
                    write_sheet.write(lines, cell_school + f, school_name)
                    write_sheet.write(lines, cell_school + 1 + f, school_start)
                    write_sheet.write(lines, cell_school + 2 + f, school_end)
                    write_sheet.write(lines, cell_school + 3 + f, school_year)
                    write_sheet.write(lines, cell_school + 4 + f, school_class)
                    write_sheet.write(lines, cell_school + 5 + f, school_spec)
                    #
                f += 6
                if title == u"Группы":
                    content = soup.find('div', {'class': 'labeled fl_l'})
                    groups = content.findAll('a')

                    for elem in groups:
                        groups_list += '%s, ' % elem.text

            content = soup.findAll('a')
            contents = u''
            if content:
                for el in content:
                    if cells.get(title):
                        contents += u'%s, ' % el.text
            else:
                content = soup.find('div', {'class': 'labeled fl_l'})
                if cells.get(title):
                    contents = content.text
            if cells.get(title):
                write_sheet.write(lines, cells.get(title), contents)

            if title == u"ВУЗ":
                content = soup.find('a')

                g = g + 6
            if title == u"Место работы":
                work_name = ''
                work_city = ''
                work_spec = ''
                work_start_date = ''
                work_end_date = ''
                div = soup.find('div', {'class': 'labeled fl_l'})
                content = soup.find('a')

                work_name = content.text
                content_a = soup.findAll('a')
                for elem in content_a:
                    if elem.text != work_name:
                        work_spec = elem.text
                div = BeautifulSoup(str(div))
                text = div.text
                text = text.replace(work_name, '')
                text = text.replace(work_spec, '')
                if text.find(',') != -1:
                    text = text.split(',')
                    work_city = text[0]

                    work_start_date = text[1]
                    if work_start_date.find('.') == -1:

                        work_end_date = work_start_date[6:10]
                        work_start_date = work_start_date[1:5]

                    else:
                        if work_start_date.find(u'до') == -1:
                            work_end_date = ''
                            work_start_date = work_start_date
                        else:
                            work_end_date = work_start_date
                            work_start_date = ''
                else:
                    work_city = text

                #
                write_sheet.write(lines, cell_work + h, work_name)
                write_sheet.write(lines, cell_work + 1 + h, work_start_date)
                write_sheet.write(lines, cell_work + 2 + h, work_end_date)
                write_sheet.write(lines, cell_work + 3 + h, work_spec)
                h += 4

        soup = BeautifulSoup(content_main)
        div = soup.findAll('div', {"class": "clear_fix miniblock"})
        if div:
            j = 0
            i = 0

            for elem in div:
                soup = BeautifulSoup(str(elem))
                title = soup.find('div', {'class': 'label fl_l'})
                title = title.text
                title = title.replace(':', '')
                content = soup.findAll('a')
                contents = u''

                if title == u"Семейное положение":
                    sex_content = soup.find('div', {'class': 'labeled fl_l'})
                    sex_content = sex_content.text

                    status = soup.find('a')
                    if sex_content.find(u'не женат') != -1 or sex_content.find(u'помовлен') != -1 or sex_content.find(
                            u'женат') != -1 or sex_content.find(u'влюблён') != -1:
                        user_sex = u'Мужской'
                    elif sex_content.find(u'не замужем') != -1 or sex_content.find(u'помолвлена') != -1 or sex_content.find(
                            u'замужем') != -1 or sex_content.find(u'влюблена') != -1 or sex_content.find(
                            u'есть друг') != -1:
                        user_sex = u'Женский'

                if cells_uni.get(title):
                    write_sheet.write(lines, cells_uni.get(title) + i, contents)
                    i = i + 6
                if content:
                    for el in content:
                        if title == u"Факультет" or title == u"Форма обучения" or title == u"Статус" or title == u"Кафедра":
                            if cells_uni.get(title):
                                j += 6
                        if cells.get(title):


                            contents += u'%s, ' % el.text

                    if title == u"Группы":


                        for elem in content:
                            groups_list += '%s, ' % elem.text
                else:
                    content = soup.find('div', {'class': 'labeled fl_l'})

                    if cells.get(title):
                        contents = content.text

                if cells.get(title):
                    write_sheet.write(lines, cells.get(title), contents)
            content = soup.find('div', {'id': 'profile_all_groups'})
            if content:
                groups = content.findAll('a')
                for elem in groups:
                    groups_list += '%s, ' % elem.text

        write_sheet.write(lines, 130, groups_list)
        write_sheet.write(lines, 6, sex_content)
        write_sheet.write(lines, 5, user_sex)
        write_sheet.write(lines, 3, fio) # Фио

        write_book.save("report.xls")
        lines += 1
        print lines
    #except urllib2.HTTPError:
    #    print lines
    #    print 'sleep'
    #    time.sleep(60)

